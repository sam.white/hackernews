# HackerNews

Stores keywords used to monitor Hacker News for topics relevant to the GitLab Secure and Defend PM Team.

## How it Works

The script stores a lastid file referencing the last item that was inspected on Hacker News.  It then retrieves items from Hacker News until it reaches the last available item.  Because new items are constantly created on Hacker News, it is recommended to schedule this job more frequently rather than less frequently.  The recommended interval is to run the job every minute to avoid accumulating a large backlog of uninspected items.

When the script starts, it attempts to pull the latest keywords.json file from this repository.  It then looks for any keywords found in the Hacker News items and accumulates an item score based on the keywords that are matched.  Scores are added together and each keyword can only count once per item, even if it appears multiple times.  The keyword search is case insensitive and will match on partial words (For example, the key word "sec" will match if the word "security" exists in the text).  Note that any items or keywords mentioned in exclusions.json will be excluded from the scoring algorithm.

The script has two criteria that need to be met before alerting on an item:
1. The total score from the specified item and any parent items must be greater than or equal to 200
1. The total score from the specified item must be greater than or equal to 25

This scoring approach allows items to meet the alerting criteria if they are in context of a broader discussion that is interesting or relevant.

Any Hacker News items that meets the alerting criteria will then trigger a Slack message notification to alert on the activity (provided that a slack webook is provided).  Additionally, if a websocket URL or file path file is provided, it will also update the webserver with the new item.

![](slack.png)

## Webserver Dashboard

In addition to the core `hackernews.py` script, this project contains a web dashboard that allow for easy review and triage of Hacker News items.  The web dashboard is currently in an Alpha state.  This area is under active development.  Contributions to the project are welcome.

![](dashboard.png)

## Installation Instructions

1. Install all dependencies: python3, the python requests, websockets, asyncio, and aiohttp libraries.  Python-pip is recommended for installing the required libraries.

```On Linux:
sudo apt-get -y install python3, python3-pip
sudo pip3 install requests
sudo pip3 install websockets
sudo pip3 install asyncio
sudo pip3 install aiohttp
```

1. Manually run the python script, or alternatively schedule a cron job to run it on a regular interval.

    ```
    sudo apt-get -y install cron
    crontab -e
    ```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Add the following line (assuming the hackernews.py script is located at /etc/hackernews/hackernews.py).


    ```
    * * * * * /etc/hackernews/hackernews.py >> /etc/hackernews/cronlog.log 2>&1
    ```


2. **[Optional Step]** To get notifications from the script, you first need to [create a Slack webhook for push notifications](https://slack.com/help/articles/115005265063-Incoming-Webhooks-for-Slack).  The webhook URL should be stored in a file called `slack` in the same folder as the python script.

3. **[Optional Step]** Setup a server to view the HackerNews notifications (Note: This feature is still in development)  These instructions are for installation on a Ubuntu/Debian based machine.  Some modifications may be necessary for other platforms.

    1. [Install Nginx](https://www.nginx.com/resources/wiki/start/topics/tutorials/install/) or another webserver.  As with any webserver installation, it is recommended to [enable SSL](https://www.techrepublic.com/article/how-to-enable-ssl-on-nginx/), [harden the server](https://geekflare.com/nginx-webserver-security-hardening-guide/), and [harden your headers](https://www.keycdn.com/blog/http-security-headers) as much as you can without breaking the application.  Please note that including the `Content-Security-Policy` Header is likely to break the application included in this project.
    2. Install Node.js.  The easiest and recommended way of doing this is by first installing [nvm](https://github.com/nvm-sh/nvm) and then using nvm to install (Node.js](https://nodejs.org/):

        ```
        curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
        Note: You will likely need to restart your shell
        nvm install node
        nvm use node
        ```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You can verify the version of node by running `node -v`.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. Configure nginx to proxy traffic to the `/ws` endpoint to the Node.js server by inserting the following text into the `server{}` section of the `/etc/nginx/sites-available/default` file.  Be sure to replace `IP_ADDRESS` with the actual IP address or hostname of your machine.

        ```
        location /ws {
                proxy_set_header   X-Forwarded-For $remote_addr;
                proxy_set_header   Host $http_host;
                proxy_set_header   Upgrade $http_upgrade;
                proxy_set_header   Connection "Upgrade";
                proxy_pass         http://IP_ADDRESS:8080;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $remote_addr;
                proxy_set_header X-Forwarded-Host $remote_addr;
                proxy_cache_bypass $http_upgrade;
        }
        ```
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4. Restart Nginx and configure it to start automatically on boot:

        ```
        sudo systemctl restart nginx
        sudo systemctl enable nginx
        ```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5. Place the files from this project's `node` folder into `/var/www/node` and run the following commands:

        ```
        sudo apt install npm
        cd /var/www/node
        npm install
        ```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6. To be able to automatically exclude keywords through the web UI, you will need a GitLab account with Maintainer access to this HackerNews project.  You will also need to [create an access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token) and store this access token in a file called `accesstoken` in the `/var/www/node` folder.  Be sure to properly protect this file after creating it:

        ```
        sudo apt-get -y install git
        sudo chown root:root /var/www/node/accesstoken
        sudo chmod 400 /var/www/node/accesstoken
        ```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7. Place the files from this project's `html` folder into `/var/www/html`.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8. Configure Node.js to start on boot

        ```
        sudo vi /etc/systemd/system/nodews.service
        ```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Insert the following text and save the file:

        ```
        [Unit]
        Description=Node.js Websocket Server
        After=network.target
        StartLimitIntervalSec=0
        
        [Service]
        type=simple
        restart=always
        RestartSec=3
        User=root
        ExecStart=node /var/www/node/server.js
        
        [Install]
        WantedBy=multi-user.target
        ```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Start your server and configure it to start on boot:

        ```
        sudo systemctl start nodews
        sudo systemctl enable nodews
        ```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9. In the same folder as your `hackernews.py` script, create a file called `wsurl` with the contents `wss://IP_ADDRESS/ws`, replacing `IP_ADDRESS` with the IP address or host name of your machine.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10. Verify that ports 443 and 8080 are not blocked by a firewall.

```
More details to be added here in the future.
Once you have a server running, place the Websocket URL in a wsurl file in the same directory as the script.
Also create a filepath file that points to a readable file on your webserver.
```
