var WebSocketServer = require('websocket').server;
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const http = require('http');
var fs = require("fs");
//var fs = require('fs');

var publishers = []
var listeners = [];
var accesstoken = "";

var server = http.createServer(function(request, response) {
//      cert: fs.readFileSync('/etc/ssl/certs/nginx-selfsigned.crt');
//      key: fs.readFileSync('/etc/ssl/private/nginx-selfsigned.key');
});
server.listen(8080, function() { });

// create the server
wsServer = new WebSocketServer({
  httpServer: server
});

function broadcast(item){
        for(i=0;i<listeners.length;i++){
                listeners[i].sendUTF(JSON.stringify({type:"item",payload:item}));
        }
}

try{
        accesstoken = fs.readFileSync("./accesstoken").toString();
} catch(err){
        console.log(err);
}

// WebSocket server
wsServer.on('request', function(request) {
  var connection = request.accept(null, request.origin);
  var type="listener";
  if(request.remoteAddress == "192.168.1.6") {
        var index = publishers.push(connection) - 1;
        type = "publisher";
  } else {
        var index = listeners.push(connection) - 1;
        var xobj = new XMLHttpRequest();
        xobj.open('GET', 'https://gitlab.com/sam.white/hackernews/-/raw/master/keywords.json', true);
        xobj.onreadystatechange = function () {
                if (xobj.readyState == 4 && xobj.status == "200") {
                        connection.sendUTF(JSON.stringify({type:"keywords",payload:JSON.parse(xobj.responseText)}));
                }
        };
        xobj.send();
        var xobj2 = new XMLHttpRequest();
        xobj2.open('GET', 'https://gitlab.com/sam.white/hackernews/-/raw/master/exclusions.json', true);
        xobj2.onreadystatechange = function () {
                if (xobj2.readyState == 4 && xobj2.status == "200") {
                        connection.sendUTF(JSON.stringify({type:"exclusions",payload:JSON.parse(xobj2.responseText)}));
                }
        };
        xobj2.send();
  }

  connection.on('message', function(message) {
    if (message.type === 'utf8') {
        try{
              message=JSON.parse(message.utf8Data)
              if(typeof message.action !== 'undefined'){

                        //Publisher
                        if(type=="publisher" && typeof message.value !== 'undefined') {
                               if(message.action == 'newItem'){
                                         broadcast(message.value);
                               }
                        }

                        //Listener
                        if(type=="listener" && typeof message.action !== 'undefined'){
                                if(message.action == 'excludekeyword' && typeof message.payload != 'undefined'){
                                        var xobj = new XMLHttpRequest();
                                        xobj.open('GET', 'https://gitlab.com/sam.white/hackernews/-/raw/master/exclusions.json', true);
                                        xobj.onreadystatechange = function () {
                                                if (xobj.readyState == 4 && xobj.status == "200") {
                                                        if(xobj.status == "200"){
                                                                data=JSON.parse(xobj.responseText);
                                                                if(data.keywords){
                                                                        data.keywords=data.keywords.concat([message.payload]);
                                                                }
                                                                payload={
                                                                        branch:"master",
                                                                        commit_message:"Auto-excluding keyword "+message.payload,
                                                                        actions:[{
                                                                                action: "update",
                                                                                file_path: "exclusions.json",
                                                                                content: JSON.stringify(data)
                                                                        }]
                                                                };
                                                                var xobj2 = new XMLHttpRequest();
                                                                xobj2.open('POST', 'https://gitlab.com/api/v4/projects/16554022/repository/commits', true);
                                                                xobj2.setRequestHeader("Content-Type", "application/json");
                                                                xobj2.setRequestHeader("Private-Token", accesstoken);
                                                                xobj2.onreadystatechange = function () {
                                                                        if (xobj2.readyState == 4) {
                                                                                if(xobj2.status == "201"){
                                                                                        connection.sendUTF(JSON.stringify({type:"keywordconfirmation",payload:message.payload}));
                                                                                } else {
                                                                                        connection.sendUTF(JSON.stringify({type:"error",payload:"Request 2 response code "+xobj2.status+" when attempting to exclude keyword "+message.payload+"."}));
                                                                                }
                                                                        }
                                                                };
                                                                xobj2.send(JSON.stringify(payload));
                                                        } else {
                                                                connection.sendUTF(JSON.stringify({type:"error",payload:"Request 1 response code "+xobj.status+" when attempting to exclude keyword "+message.payload+"."}));
                                                        }
                                                }
                                        };
                                        xobj.send();
                                } else if(message.action == 'dismissid' && typeof message.payload != 'undefined'){
                                        fs.readFile("/var/www/html/findings.json", "utf-8", (err, data) => {
                                                if(err) {
                                                        connection.sendUTF(JSON.stringify({type:"error",payload:"Unable to read findings.json file."}));
                                                        console.log(err);
                                                }
                                                if(data.length==0) data="[";
                                                data=data+"]";
                                                findings=JSON.parse(data);
                                                found=false;
                                                for(i=0;!found && i<findings.length;i++){
                                                    if(findings[i].id && findings[i].id==message.payload) {
                                                            findings.splice(i,1);
                                                            found=true;
                                                    }
                                                }
                                                if(found) {
                                                        data=JSON.stringify(findings);
                                                        if(findings.length==0) data="";
                                                        else data=data.substring(0,data.length-1);
                                                        fs.writeFile("/var/www/html/findings.json", data, (err) => {
                                                                if(err) {
                                                                        connection.sendUTF(JSON.stringify({type:"error",payload:"Unable to write to findings.json file."}));
                                                                        console.log(err);
                                                                } else {
                                                                        connection.sendUTF(JSON.stringify({type:"dismissconfirmation",payload:message.payload}));
                                                                }
                                                        });
                                                } else {
                                                        connection.sendUTF(JSON.stringify({type:"error",payload:"Unable to find "+message.payload+" in findings.json file."}));
                                                }
                                        });
                                }
                        }
                }
        } catch (e) {
		if(e instanceof SyntaxError){
                        connection.sendUTF("Invalid JSON");
                } else {
                        connection.sendUTF("Error");
                }
        }
    }
  });

  connection.on('close', function(connection) {
        var found=false;
        for(i=0;i<publishers.length && !found;i++){
                if(publishers[i]==connection){
                        found=true;
                        publishers.splice(i,1);
                }
        }
        for(i=0;i<listeners.length && !found;i++){
                if(listeners[i]==connection){
                        found=true;
                        listeners.splice(i,1);
                }
        }
  });
});
