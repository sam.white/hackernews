#!/usr/bin/env python3
#import commands
import requests
import json as JSON
import os.path
import ssl
import websockets
import asyncio
import aiohttp
import subprocess

dir = os.path.dirname(os.path.abspath(__file__))

try:
	if os.path.exists(dir+'/cronlog.log') and os.path.isfile(dir+'/cronlog.log'):
		with open(dir+'/cronlog.log','w') as file:
			file.truncate()
			file.close()
except:
	print("Unable to clear log")

p1 = subprocess.Popen("ps aux".split(), stdout=subprocess.PIPE)
p2 = subprocess.Popen("grep -e hackernews.py".split(), stdin=p1.stdout, stdout=subprocess.PIPE)
p3 = subprocess.Popen("grep -v grep".split(), stdin=p2.stdout, stdout=subprocess.PIPE)
p4 = subprocess.Popen(["grep","-v","\/bin\/sh"], stdin=p3.stdout, stdout=subprocess.PIPE)
p5 = subprocess.run(["awk","{print $2}"], stdin=p4.stdout, stdout=subprocess.PIPE)

pids=p5.stdout.splitlines()
for pid in pids:
	pid=int(pid.decode("utf-8"))
	if pid!=os.getpid():
		print("Already running under pid: "+str(pid)+" this pid:"+str(os.getpid()))
		exit()

try:
	max=requests.get('https://hacker-news.firebaseio.com/v0/maxitem.json')
	max=int(max.text)
except:
	print("Unable to initialize maxid")
	exit()

slack = ""

try:
	with open(dir+'/slack','r') as file:
		slack = file.read().replace('\n','')
		file.close()
except:
	print("Unable to locate Slack webhook URL")

wsurl = ""

try:
	with open(dir+'/wsurl','r') as file:
		wsurl = file.read().replace('\n','')
		file.close()
except:
	print("Unable to locate Websocket URL")

try:
	data = requests.get('https://gitlab.com/sam.white/hackernews/-/raw/master/keywords.json')
	with open(dir+'/keywords.json','w') as file:
		file.write(data.text)
		file.truncate()
		file.close()
except:
	print("Unable to update keywords")

try:
	with open(dir+'/keywords.json','r') as file:
		keywords = file.read().replace('\n','')
		file.close()
except IOError:
	print("Unable to read keywords")
	exit()

try:
	exclusions = requests.get('https://gitlab.com/sam.white/hackernews/-/raw/master/exclusions.json')
	with open(dir+'/exclusions.json','w') as file:
		file.write(exclusions.text)
		file.truncate()
		file.close()
except:
	print("Unable to update exclusions")

try:
	with open(dir+'/exclusions.json','r') as file:
		exclusions = file.read().replace('\n','')
		file.close()
except IOError:
	print("Unable to read exclusions")
	exit()

if not keywords:
	exit()

try:
	keywords=JSON.loads(keywords)
	exclusions=JSON.loads(exclusions)
except:
	print("Incorrect JSON")
	exit()

try:
	if not os.path.exists(dir+'/lastid') or not os.path.isfile(dir+'/lastid'):
		with open(dir+'/lastid','w') as file:
			file.write(str(max))
			file.close
except:
	print("Unable to initialize lastid")
	exit()

try:
	with open(dir+'/lastid','r') as file:
		id = file.read().replace('\n','')
		file.close()
	if not id:
		exit()
	id = int(id)
except:
	print("Can't read last id")
	exit()

async def scoreid(session, id):
	global keywords,exclusions,dir,slack,wsurl

	try:
		response = await session.get('https://hacker-news.firebaseio.com/v0/item/'+str(id)+'.json')
		r = await response.json()
		if r is None or 'id' not in r:
			return [id, -2, "response is None or no id in r"]
		scoreA = 0
		scoreB = 0
		matchtext=""
		matches={}
		textA = ""
		textB = ""
		textid = str(r['id'])
		itemarr = []
		item = {}

		if r['id'] not in exclusions['ids']:
			item['id']=r['id']
			if 'title' in r:
				textA = r['title']
				item['title'] = r['title']
			if 'text' in r:
				textA = textA + " " + r['text']
				item['text'] = r['text']
			
			textB = textA

			while 'parent' in r:
				response = await session.get('https://hacker-news.firebaseio.com/v0/item/'+str(r['parent'])+'.json')
				r = await response.json()
				if r is None or 'id' not in r:
					return [id, -2, "response is None or no id in r"]
				itemarr.append(item)
				item={}
				if r['id'] not in exclusions['ids']:
					item['id']=r['id']
					if 'title' in r:
						textA = textA + " " + r['title']
						item['title'] = r['title']
					if 'text' in r:
						textA = textA + " " + r['text']
						item['text'] = r['text']
				else:
					item['id']=r['id']
					item['excluded']=True
					if 'title' in r:
						item['title'] = r['title']
					if 'text' in r:
						item['text'] = r['text']

			itemarr.append(item)
			item = {}
			i=len(itemarr)-1
			while i>=0:
				if i==len(itemarr)-1:
					item=itemarr[i]
				else:
					tempitem=item
					item=itemarr[i]
					item['parent']=tempitem
				i -= 1			

			for excepttext in exclusions['keywords']:
				textA = textA.replace(excepttext,"")
				textB = textB.replace(excepttext,"")

			for key in keywords:
				if key.lower() in textA.lower():
					scoreA = scoreA + int(keywords[key])
					matches[key] = True
				if key.lower() in textB.lower():
					scoreB = scoreB + int(keywords[key])

			for key in matches:
				if matchtext=="":
					matchtext = key
				else:
					matchtext = matchtext + ", " + key

			print(textid+": "+ str(scoreA)+", "+str(scoreB))
			
			item['scoreA']=scoreA
			item['scoreB']=scoreB

			if scoreA>=200 and scoreB>=25:
				if slack != "":
					headers={
						"Content-Type": "application/json"
					}
					payload={
						"text": "<@U0157HHJ5E3> Hacker News Score: "+str(scoreA)+"\nKeywords: "+matchtext+"\nhttps://news.ycombinator.com/item?id="+textid
					}
					data = JSON.dumps(payload)
					await session.post(slack,data=JSON.dumps(payload),headers=headers)
				if wsurl !="":
					try:
						context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
						context.check_hostname = False
						context.verify_mode = ssl.CERT_NONE
						ws = await websockets.connect(wsurl, ssl=context)
						await ws.send('{ "action":"newItem", "value":' + JSON.dumps(item) + ' }')
					except Exception as error:
						print("Lost websocket connection")
						ws = False
						raise error
				try:
					with open("/var/www/html/findings.json",'a') as file:
						if os.path.getsize("/var/www/html/findings.json")>0:
							file.write(","+JSON.dumps(item))
							file.close()
						else:
							file.write("["+JSON.dumps(item))
							file.close()
				except Exception as error:
					print("Unable to write to file")
					raise error
	except Exception as e:
		return [id, -1, e]
	return [id, scoreA, ""]

async def processScores(idlist, lastid):
	global dir
	async with aiohttp.ClientSession() as session:
		tasks = []
		for id in idlist:
			task = asyncio.ensure_future(scoreid(session, id))
			tasks.append(task)
		results = await asyncio.gather(*tasks, return_exceptions=True)
		print(results)

		## Try again before giving up
		tasks = []
		for result in results:
			if result[1]<0:
				task = asyncio.ensure_future(scoreid(session, id))
				tasks.append(task)
		results = await asyncio.gather(*tasks, return_exceptions=True)
		print(results)

		found = False
		for result in results:
			if result[1]<0:
				found = True

		if not found:
			try:
				with open(dir+'/lastid','w') as file:
					file.write(str(lastid))
					file.truncate()
					file.close()
			except:
				print("Unable to update maxid")
				exit()

idlist = list(range(id,max))
asyncio.get_event_loop().run_until_complete(processScores(idlist,max))

