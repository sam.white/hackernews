FROM ubuntu/nginx:latest

RUN apt update && \
    apt upgrade -y && \
    apt install -y make sudo git curl python3 python3-pip cron systemctl && \
    rm /usr/lib/python3.11/EXTERNALLY-MANAGED
RUN useradd -m hackernews && \
    usermod -aG sudo hackernews && \
    echo "hackernews:hackernews" | chpasswd && \
    mkdir /var/www/node && \
    chown -R hackernews /var/www
COPY accesstoken /var/www/node/accesstoken
RUN chown root:root /var/www/node/accesstoken && \
    chmod 400 /var/www/node/accesstoken && \
    echo -e "[Unit]\nDescription=Node.js Websocket Server\nAfter=network.target\nStartLimitIntervalSec=0\n\n[Service]\ntype=simple\nrestart=always\nRestartSec=3\nUser=root\nExecStart=node /var/www/node/server.js\n\n[Install]\nWantedBy=multi-user.target" > /etc/systemd/system/nodews.service && \
    systemctl daemon-reload && \
    systemctl enable nodews
USER hackernews
WORKDIR /home/hackernews

RUN pip3 install requests websockets asyncio aiohttp && \
    git clone https://gitlab.com/sam.white/hackernews.git && \
    echo "* * * * * /etc/hackernews/hackernews.py >> /etc/hackernews/cronlog.log 2>&1" > crontab.txt && \
    crontab crontab.txt && \
    rm -f crontab.txt && \
    touch ~/.bashrc && chmod +x ~/.bashrc && \
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash && \
    export NVM_DIR=~/.nvm && \
    . ~/.nvm/nvm.sh && \
    . ~/.bashrc && \
    nvm install node && \
    cp ./hackernews/html/* /var/www/html/ && \
    cp ./hackernews/node/* /var/www/node/ && \
    cd /var/www/node && \
    npm install

EXPOSE 443 8080
CMD ["nginx" "-g" "daemon off;"]